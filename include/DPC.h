////////////////////////////////////////////////////////////////////////////// 
/// 
/// \mainpage			DPC Library
///
///--------------------------------------------------------------------------- 
/// \section det_sec Details
/// - FileName		:	DPC.h
/// - Dependencies	:   None
/// - Compiler		:   C/C++
/// - Company		:   Copyright (C) Aurea Technology
/// - Author		:	Matthieu Grovel
///
///	\section des_sec Description
///	This header document provide the prototypes and descriptions of all functions    
///	integrated on both dynamic (.dll) and static libraries (.lib) files.
/// Those functions allows to control DPC device also named "DPC"
/// 
/// \section imp_sec Important
///
/// More or less functions are available according to the device type.	
///	The compatibility depends on the device part number recovered by
///	the "GetSystemVersion" function.								
///	Please see notes functions to check the compatibility with your device:  	
///   -> version compatibility:  PN_DPC_x_xx_xx_xx 	
/// 
/// \section rev_sec Revisions
///
/// v1.0 (30/01/2023)
/// - First release
/// 
////////////////////////////////////////////////////////////////////////////// 

#ifndef _DPC_H
#define _DPC_H

#ifndef DOXYGEN_SHOULD_SKIP_THIS

// Channel
#define CH_1	0
#define CH_2	1

// Define LIB_CALL for any platform
#if defined _WIN32 || defined __CYGWIN__
	#if defined WIN_EXPORT
		#ifdef __GNUC__
		#define LIB_CALL __attribute__ ((dllexport))
		#else
		#define LIB_CALL __declspec(dllexport) __cdecl
		#endif
	#else
		#ifdef __GNUC__
		#define LIB_CALL __attribute__ ((dllimport))
		#else
		#define LIB_CALL __declspec(dllimport) 
		#endif
	#endif
#else
	#if __GNUC__ >= 4
	#define LIB_CALL __attribute__ ((visibility ("default")))
	#else
	#define LIB_CALL
	#endif
#endif

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <Windows.h>
#include "conio.h"
#define delay(x) Sleep(x)
#elif __unix
#include <cstring>
#include <unistd.h>
#define delay(x) usleep(x*1000)
#else
#include <unistd.h>
#define delay(x) usleep(x*1000)
#endif

#ifdef _WIN32 
#define secure_strtok strtok_s
#else
#define secure_strtok strtok_r
#endif	

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

#if defined(__cplusplus)
extern "C" {
#endif

 /////////////////////////////////////////////////////////////////////////////////////
/// \fn			short DPC_getLibVersion(unsigned short *value)
///	\brief		Get the librarie version
/// \details	Return the version librarie in format 0x0000MMmm \n
///				with: MM=major version \n
///					  mm=minor version			  
///
/// \param		*value	return lib version by pointer \n
///						Format: 0xMMmm		\n
///						with:	MM: major version	\n
///								mm: minor version
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
short LIB_CALL DPC_getLibVersion(unsigned short* value);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short DPC_listDevices(char** devices, short *number)
/// \brief		List Aurea Technology devices connected
/// \details	List Aurea Technology devices connected
///	\note		Mandatory to do before any other action on the system device.
///
/// \param		**devices pointer to the table buffer which contain list of devices connected \n
///						 Output format: "deviceName - serialNumber" \n
///						 Example:									\n
///						 devices[0]="DPC - SN_xxxxx1xxxxx\r\n"		\n
///						 devices[1]="DPC - SN_xxxxx6xxxxx\r\n"		\n
///
/// \param		*number pointer to the number devices connected
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL DPC_listDevices(char** devices, short* number);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short DPC_openDevice(short iDev)
/// \brief		Open and initialize DPC device
/// \details	Open USB connection and initialize internal configuration
///	\note		Mandatory to do before any other action on the system device.
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL DPC_openDevice(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn	void	short DPC_closeDevice(short iDev)
///	\brief		Close DPC device
/// \details	Close USB connection of previously DPC opened. 
///	\note		Mandory to do after each end of system transfer.
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_closeDevice(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getSystemVersion(short iDev, char *version)
/// \brief		Get system version
/// \details	Get system version: Serial number, product number and firmware version
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
///
/// \param		version	Pointer to the buffer which receive the system version. \n
///						String format: SN_'serialNumber':PN_'ProductNumber':'FirmwareVersion'\n
///						The receive buffer size must be of 64 octets min.
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getSystemVersion(short iDev, char *version);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getSystemFeature(short iDev, short iFeature, short* value)
///	\brief		Get system feature
/// \details	Read EEPROM to recovery one feature of system
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		iFeature	0: detection speed 		\n
///							1: module type 			\n
///							2: module performance	\n
///							3: reserved				\n
///							4: reserved				\n
///							5: Output format type			
///
///  \param		*value	pointer to the feature value (format: short)
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL DPC_getSystemFeature(short iDev, short iFeature, short* value);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getSystemHardwareVersion(short iDev, short card, unsigned short *version, unsigned short *model)
///	\brief		Get system hardware card version and model
///	\details	
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		card	0 : uMCE	\n
///						1 : MTM	\n
///						2 : MCT		
///
/// \param		version	pointer on value of card's version
/// 
/// \param		model	pointer on value of card's model
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL DPC_getSystemHardwareVersion(short iDev, short card, unsigned short* version, unsigned short* model);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_setEfficiency(short iDev, unsigned char channel, short efficiency)
///	\brief		Set efficiency
/// \details	Set APD effciency value (in %).
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
///
///	\param		efficiency	Efficiency value in % and in multiple of 5
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
///
short LIB_CALL DPC_setEfficiency(short iDev, unsigned char channel, short efficiency);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getEfficiency(short iDev, unsigned char channel, short *efficiency)
/// \brief		Get efficiency
/// \details	Get actual APD efficiency value (in %).
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
/// \param		*efficiency	pointer to the efficiency value (format: double)
///
///	\return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL DPC_getEfficiency(short iDev, unsigned char channel, short *efficiency);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_setDeadTime(short iDev, unsigned char channel, double deadTime)
///	\brief		Set deadtime
/// \details	Set APD deadtime value (in us).
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
///
///	\param		deadTime DeadTime value in us (format: double) 
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
///
short LIB_CALL DPC_setDeadTime(short iDev, unsigned char channel, double deadTime);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getDeadTime(short iDev, unsigned char channel, double *deadTime)
///	\brief		Get deadtime
/// \details	Get actual APD deadtime value (in us).
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
/// \param		*deadTime	pointer to the deadTime value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
///
short LIB_CALL DPC_getDeadTime(short iDev, unsigned char channel, double *deadTime);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getCLKCountData(short iDev, unsigned char channel, unsigned long *CLK, unsigned long *Count)
///	\brief		Get clock and count data
/// \details	Get both clock value and photons count.
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
///	
/// \param		CLK	Pointer to the APD clock value (format: decimal)	
/// 
/// \param		Count	Pointer to the APD count value (format: decimal)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getCLKCountData(short iDev, unsigned char channel, unsigned long *CLK, unsigned long *Count);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_saveAllSettings(short iDev)
///	\brief		Save all parameters
/// \details	Save all parameters (deadtime, detection mode, ...).
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL DPC_saveAllSettings(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_factorySettings(short iDev)
///	\brief		System factory settings
/// \details	Set all parameters with factory settings.
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL DPC_factorySettings(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short DPC_setCountingRate(short iDev, double rate)
/// \brief		Set the rate of sending photons counted
/// \details	Set the rate of sending photons counted. Rate value between 0.1s 
///				to 10s.
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
///
/// \param		rate	Rate value in s (format: double) \n 
///						Value between 0.1 to 10.0s with step of 0.1s
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL DPC_setCountingRate(short iDev, double rate);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short DPC_getCountingRate(short iDev, double *rate)
/// \brief		Get counting rate value
/// \details	Get the rate of sending photons counted. Rate value between 0.1s 
///				to 10s.
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
///
/// \param      rate   Pointer to the current rate value (format: double).
///
/// \return	
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL DPC_getCountingRate(short iDev, double *rate);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getEfficiencyRange(short iDev, char *range)
///	\brief		Get efficiency range
///	\details	Send all APD efficiency available values
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		range	Pointer to the string buffer of the efficiency range.
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getEfficiencyRange(short iDev, char *range);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short  DPC_getDeadTimeRange(short iDev, unsigned char channel, double *MinVal, double *MaxVal)
///	\brief		Get deadtime range
///	\details	Get min and max values of the deadtime range
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
///	\param		*MinVal	pointer to the current deadtime min value (format: double) 
///
///	\param		*MaxVal	pointer to the current deadtime max value (format: double)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL DPC_getDeadTimeRange(short iDev, unsigned char channel, double* MinVal, double* MaxVal);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_resetSystem(short iDev)
///	\brief		Reset system
///	\details	Reset system
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_resetSystem(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getBodySocketTemp(short iDev, double *bodyTemp)
///	\brief		Get body socket system temperature
///	\details	Get body socket system temperature
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		*bodyTemp	pointer to the body temperature value (format: double)
/// 
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getBodySocketTemp(short iDev, double *bodyTemp);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getSystemAlarms(short iDev, char *alarm)
///	\brief		Get System Alarms
///	\details	Get active alarm detected by system
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
///	\param		alarm	Pointer to the buffer which receive the alarm description. \n
///						The receive buffer size must be of 64 octets min.
///
/// \return
///				1 : Alarm detected	   \n
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getSystemAlarms(short iDev, char *alarm);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_setOutputState(short iDev, unsigned char channel, short state)
/// \brief		Set output state
///	\details	Set output state
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
///	\param		state	output state: 1=enabled; 0=disabled
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL DPC_setOutputState(short iDev, unsigned char channel, short state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getOutputState(short iDev, unsigned char channel, short *state)
/// \brief		Get output state
///	\details	Get output state
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
/// \param		*state	pointer to the output state (format: short)
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL DPC_getOutputState(short iDev, unsigned char channel, short *state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_setOutputFormat(short iDev, unsigned char channel, short format)
///	\brief		Set output detection format
///	\details	Set output detection format in numerical, analogic or NIM
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
/// \param		format	0: numerical \n
///						1: analogic \n
///						2: NIM 
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_setOutputFormat(short iDev, unsigned char channel, short format);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getOutputFormat(short iDev, unsigned char channel, short *format)
///	\brief		Get output detection format 
///	\details	Get output detection format analog, numeric or NIM
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
/// \param		*format	pointer to the output format (format: short) \n
/// 					0: numerical \n
///						1: analogic \n
///						2: NIM 
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getOutputFormat(short iDev, unsigned char channel, short *format);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_setIntegTime(short iDev, unsigned char channel, double timeInMs)
///	\brief		Set integration time 
///	\details	Set integration time of detections counted for analog output format
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
///
/// \param		timeInMs	integration time between 0.1 to 10000.0 ms
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_setIntegTime(short iDev, unsigned char channel, double timeInMs);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getIntegTime(short iDev, unsigned char channel, double* timeInMs)
///	\brief		Get integration time 
///	\details	Get current integration time (in ms) of analog output format 
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
/// \param		*timeInMs	pointer to the time value in ms (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getIntegTime(short iDev, unsigned char channel, double* timeInMs);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_setAnalogOutGain(short iDev, unsigned char channel, double gain)
///	\brief		Set analog gain  
///	\details	Set gain of analogic format output
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
///
/// \param		gain	Analogic gain between 0.1 to 100.0
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_setAnalogOutGain(short iDev, unsigned char channel, double gain);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getAnalogOutGain(short iDev, unsigned char channel, double *gain)
///	\brief		Get analog gain
///	\details	Get gain of analogic format output 
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
/// \param		*gain	pointer to the gain value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getAnalogOutGain(short iDev, unsigned char channel, double *gain);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_setDetectionMode(short iDev, unsigned char channel, short mode)
///	\brief		Set  detection mode 
///	\details	Set detection mode in continuous or gated mode
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
///
/// \param		mode	0: continuous	\n
///						1: gated
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_setDetectionMode(short iDev, unsigned char channel, short mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getDetectionMode(short iDev, unsigned char channel, short *mode)
///	\brief		Get detection mode 
///	\details	Get detection mode in continuous or gated mode 
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
/// \param		*mode	pointer to the detection mode (format: short)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getDetectionMode(short iDev, unsigned char channel, short *mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_setInputVoltageThreshold(short iDev, unsigned char channel, double voltage)
///	\brief		Set input voltage threshold 
///	\details	Set voltage threshold for system pulses input
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
/// \param		voltage	Voltage threshold between 0.2 to 4.0 V
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_setInputVoltageThreshold(short iDev, unsigned char channel, double voltage);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getInputVoltageThreshold(short iDev, unsigned char channel, double *voltage)
///	\brief		Get input voltage threshold
///	\details	Get voltage threshold of system pulses input
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
/// \param		*voltage	pointer to the voltage value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getInputVoltageThreshold(short iDev, unsigned char channel, double *voltage);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_setDetLimitThreshold(short iDev, unsigned char channel, double detections)
///	\brief		Set detections limit threshold 
///	\details	Set detections limit threshold after which the system power off 
///				the detector to protect it.
/// 
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
///
/// \param		detections	Number of photons detected between 0.1 to 25.0 M photons/s
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_setDetLimitThreshold(short iDev, unsigned char channel, double detections);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getDetLimitThreshold(short iDev, unsigned char channel, double* detections)
///	\brief		Get detections limit threshold
///	\details	Get detections limit threshold after which the system power off 
///				the detector to protect it.
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
///
/// \param		*detections	pointer to the detection value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getDetLimitThreshold(short iDev, unsigned char channel, double* detections);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getOutputVoltage(short iDev, unsigned char channel, double *voltage)
///	\brief		Get output voltage
/// \details	Get the current output voltage in analog format output
///		
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		channel	channel to use (CH_1 : 0 or CH_2 : 1)
/// 
/// \param		*voltage	Pointer to the voltage value (format: double)
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
///
short LIB_CALL DPC_getOutputVoltage(short iDev, unsigned char channel, double *voltage);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_setFanState(short iDev, short state)
///	\brief		Set Fan state
///	\details	Set Fan On or Off
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		state	0 : OFF, 1 : ON
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
//short LIB_CALL DPC_setFanState(short iDev, short state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getFanState(short iDev, short *state)
///	\brief		Get Fan state
///	\details	Get Fan On or Off
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		*state	pointer to the fan state (format: short)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
//short LIB_CALL DPC_getFanState(short iDev, short* state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_setLedState(short iDev, short state)
///	\brief		Set Led state
///	\details	Set Led On or Off
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		state	0 : OFF, 1 : ON
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL DPC_setLedState(short iDev, short state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_getLedState(short iDev, short *state)
///	\brief		Get Led state
///	\details	Get Led On or Off
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
/// 
/// \param		*state	pointer to the Led state (format: short)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL DPC_getLedState(short iDev, short* state);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short DPC_identify(short iDev)
///	\brief		Identify the device
///	\details	make device led blink
///
/// \param		iDev	Device index indicate by "DPC_listDevices" function
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
short LIB_CALL DPC_identify(short iDev);

#if defined(__cplusplus)
}
#endif

#endif
