.. _installation:

Software Installation Guide
===========================

The following section describes how to install the DPC software on Windows, MacOS and Linux operating systems.

Windows
-------

Operating Systems Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	- Windows 7 or higher

	- Application and examples are working on 32bit and 64bit systems.
	

Installation Step
^^^^^^^^^^^^^^^^^

	#. Run the setup file locate in provided directory.

	#. Connect DPC device to your computer with the USB cable.

	#. Start Aurea-DPC application or start Aurea-Launcher and then click on your device to use the software. 


MacOS
-----

	- Aurea-Launcher Installation :

		#. Double click on Aurea-Launcher.dmg file.

		#. Drag Aurea-Launcher in the Applications folder.

	- Aurea-DPC Installation :

		#. Double click on Aurea-DPC.dmg file.

		#. Drag Aurea-DPC in the Applications folder

		#. Connect DPC device to your computer with the USB cable.

		#. Launch Aurea-DPC or Aurea-Launcher application by clicking on it.

Linux
-----
	
	- Aurea-Launcher Installation :

		#. Unzip Aurea-Launcher-package.zip

		#. Go to Aurea-Launcher-package/Aurea-Launcher and double-click on Aurea-Launcher-Installer.

		#. Follow the installer instructions and make sure to install all Aurea Technology software in the same directory.

	- Aurea-DPC Installation :

		#. Unzip Aurea-DPC-package.zip

		#. Go to Aurea-DPC-package/Aurea-DPC and double-click on Aurea-DPC-Installer.

		#. Follow the installer instructions and make sure to install all Aurea Technology software in the same directory.

		#. Connect DPC device to your computer with the USB cable.

		#. Launch Aurea-DPC or Aurea-Launcher application by executing the following command in the installation directory.

	.. code-block:: console

	    ./Aurea-Launcher.sh

	.. code-block:: console

	    ./Aurea-DPC.sh