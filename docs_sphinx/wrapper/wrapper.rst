.. _wrapper:

C++ Wrapper
===========

Wrapper Advantage
~~~~~~~~~~~~~~~~~

A C++ wrapper has been created for several reasons.
	- To make DPC functions easy to use.
	- To allow multiple DPC device control in the same application and at the same time.
	- To link Dynamic Library inside C++ code and not in the project configuration.

.. note::

	Except OpenDevice function, you do not need to specify iDev when using DPC wrapper function.

	For example function ``DPC_getCLKCountData(short iDev, unsigned char channel, unsigned long *CLK, unsigned long *Count)`` can be replace by ``ObjectName.GetCLKCountData(unsigned char channel, unsigned long *CLK, unsigned long *Count)``

C++ code
~~~~~~~~

Here is an example of how to use this wrapper to recover data from 2 DPC :

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "DPC_wrapper.h"
	#include "DPC.h"

	// Select shared library compatible to current operating system
	#ifdef _WIN32
	#define DLL_PATH L"DPC.dll"
	#elif __unix
	#define DLL_PATH "DPC.so"
	#else
	#define DLL_PATH "DPC.dylib"
	#endif

	int main(int argc, const char* argv[]) {
		short iDev = 0;
		short ret;
		char* devicesList[10];
		short numberDevices;
		unsigned long CLK = 0, Count = 0;
		unsigned long CLK2 = 0, Count2 = 0;

		// Instancie the device from wrapper
		DPC_wrapper DPC0(DLL_PATH);
		DPC_wrapper DPC1(DLL_PATH);

	    /*    ListDevices function    */
	    // List Aurea Technology devices: MANDATORY BEFORE EACH OTHER ACTION ON THE SYSTEM
	    if (DPC0.ListDevices(devicesList, &numberDevices) == 0) { 
	        if (numberDevices == 0){            
	            cout << endl << "    Please connect AT device !" << endl << endl;
	            do {
	                delay(500);
	                DPC0.ListDevices(devicesList, &numberDevices);
	            } while (numberDevices == 0);
	        }
	    }

		// Open communication with device 0
		printf(" -%u: %s\n", 0, devicesList[0]);
		DPC0.OpenDevice(0);
		printf("\n DPC %d-> Communication Open\n\n", 0);

		// Open communication with device 1
		printf(" -%u: %s\n", 1, devicesList[1]);
		DPC1.OpenDevice(1);
		printf("\n DPC %d-> Communication Open\n\n", 1);

		// Recover Clock and Photons count from channel 1 for both devices
		DPC0.GetCLKCountData(CH_1, &CLK, &Count);
		printf("\n\nDPC 0 -> Clock: %7lu	Hz	Counts : %7lu", CLK, Count);
		DPC1.GetCLKCountData(CH_1, &CLK2, &Count2);
		printf("\nDPC 1 -> Clock: %7lu	Hz	Counts : %7lu", CLK2, Count2);

		// Wait some time
		delay(2000);

		/*    CloseDevice function    */
		// Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
		if (DPC0.CloseDevice() == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;
		if (DPC1.CloseDevice() == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;

		// Call class destructor
		DPC0.~DPC_wrapper();
		DPC1.~DPC_wrapper();

		return 0;
	}
