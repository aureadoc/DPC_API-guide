.. _api_functions:


Library information
===================

DPC_getLibVersion
-----------------

.. doxygenfunction:: DPC_getLibVersion
   :project: DPC - API guide




Connection Functions
====================

DPC_listDevices
---------------

.. doxygenfunction:: DPC_listDevices
   :project: DPC - API guide

DPC_openDevice
--------------

.. doxygenfunction:: DPC_openDevice
   :project: DPC - API guide

DPC_closeDevice
---------------

.. doxygenfunction:: DPC_closeDevice
   :project: DPC - API guide





Save and Reset Settings
=======================

DPC_saveAllSettings
-------------------

.. doxygenfunction:: DPC_saveAllSettings
   :project: DPC - API guide

DPC_factorySettings
-------------------

.. doxygenfunction:: DPC_factorySettings
   :project: DPC - API guide





Reboot System
=============

DPC_resetSystem
---------------

.. doxygenfunction:: DPC_resetSystem
   :project: DPC - API guide





Device Information
==================

DPC_getSystemVersion
--------------------

.. doxygenfunction:: DPC_getSystemVersion
   :project: DPC - API guide

DPC_getSystemFeature
--------------------

.. doxygenfunction:: DPC_getSystemFeature
   :project: DPC - API guide

DPC_getSystemHardwareVersion
----------------------------

.. doxygenfunction:: DPC_getSystemHardwareVersion
   :project: DPC - API guide





Recover Parameters Range
========================

DPC_getEfficiencyRange
----------------------

.. doxygenfunction:: DPC_getEfficiencyRange
   :project: DPC - API guide

DPC_getDeadTimeRange
--------------------

.. doxygenfunction:: DPC_getDeadTimeRange
   :project: DPC - API guide




Set and Get Parameters
======================

DPC_setEfficiency
-----------------

.. doxygenfunction:: DPC_setEfficiency
   :project: DPC - API guide

DPC_getEfficiency
-----------------

.. doxygenfunction:: DPC_getEfficiency
   :project: DPC - API guide

DPC_setDeadTime
---------------

.. doxygenfunction:: DPC_setDeadTime
   :project: DPC - API guide

DPC_getDeadTime
---------------

.. doxygenfunction:: DPC_getDeadTime
   :project: DPC - API guide

DPC_setCountingRate
-------------------

.. doxygenfunction:: DPC_setCountingRate
   :project: DPC - API guide

DPC_getCountingRate
-------------------

.. doxygenfunction:: DPC_getCountingRate
   :project: DPC - API guide

DPC_setOutputState
------------------

.. doxygenfunction:: DPC_setOutputState
   :project: DPC - API guide

DPC_setIntegTime
----------------

.. doxygenfunction:: DPC_setIntegTime
   :project: DPC - API guide

DPC_getIntegTime
----------------

.. doxygenfunction:: DPC_getIntegTime
   :project: DPC - API guide

DPC_setAnalogOutGain
--------------------

.. doxygenfunction:: DPC_setAnalogOutGain
   :project: DPC - API guide

DPC_getAnalogOutGain
--------------------

.. doxygenfunction:: DPC_getAnalogOutGain
   :project: DPC - API guide

DPC_setDetectionMode
--------------------

.. doxygenfunction:: DPC_setDetectionMode
   :project: DPC - API guide

DPC_getDetectionMode
--------------------

.. doxygenfunction:: DPC_getDetectionMode
   :project: DPC - API guide

DPC_setInputVoltageThreshold
----------------------------

.. doxygenfunction:: DPC_setInputVoltageThreshold
   :project: DPC - API guide

DPC_getInputVoltageThreshold
----------------------------

.. doxygenfunction:: DPC_getInputVoltageThreshold
   :project: DPC - API guide

DPC_getDetLimitThreshold
------------------------

.. doxygenfunction:: DPC_getDetLimitThreshold
   :project: DPC - API guide





Monitoring Functions
====================

DPC_getCLKCountData
-------------------

.. doxygenfunction:: DPC_getCLKCountData
   :project: DPC - API guide

DPC_getBodySocketTemp
---------------------

.. doxygenfunction:: DPC_getBodySocketTemp
   :project: DPC - API guide

DPC_getOutputVoltage
--------------------

.. doxygenfunction:: DPC_getOutputVoltage
   :project: DPC - API guide

DPC_getSystemAlarms
-------------------

.. doxygenfunction:: DPC_getSystemAlarms
   :project: DPC - API guide





Hardware Control
================

DPC_setLedState
---------------

.. doxygenfunction:: DPC_setLedState
   :project: DPC - API guide

DPC_getLedState
---------------

.. doxygenfunction:: DPC_getLedState
   :project: DPC - API guide


DPC_identify
------------

.. doxygenfunction:: DPC_identify
   :project: DPC - API guide